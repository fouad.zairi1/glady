DROP TABLE IF EXISTS USERS cascade;
create table users (
id int auto_increment primary key,
last_name varchar(50) not null,
first_name varchar(50) not null
);

DROP TABLE IF EXISTS card cascade;
create table card (
id int auto_increment primary key,
amount int not null,
card_type varchar(50) not null,
user_id int not null,
company_id int not null,
distribution_date date not null,
expiration_date date not null
);

DROP TABLE IF EXISTS COMPANY cascade;
create table company (
id int auto_increment primary key,
name varchar(50) not null,
balance int not null
);