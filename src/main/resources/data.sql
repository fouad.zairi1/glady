insert into users (last_name, first_name) values ('Pinkman', 'Jesse');
insert into users (last_name, first_name) values ('White', 'Walter');
insert into users (last_name, first_name) values ('Fring', 'Gustavo');

insert into company (name, balance) values ('Albuquerque', 5900);
insert into company (name, balance) values ('Denver', 7500);
insert into company (name, balance) values ('Santa Fe', 1300);
insert into company (name, balance) values ('Mesa Verde', 6900);
insert into company (name, balance) values ('Las Vegas', 2700);

insert into card (amount, card_type, user_id, company_id, distribution_date, expiration_date) values (100, 'GIFT', 1, 1, current_date, current_date + 341);
insert into card (amount, card_type, user_id, company_id, distribution_date, expiration_date) values (120, 'MEAL', 1, 2, current_date, current_date + 15);
insert into card (amount, card_type, user_id, company_id, distribution_date, expiration_date) values (140, 'GIFT', 1, 1, current_date, current_date - 60);
insert into card (amount, card_type, user_id, company_id, distribution_date, expiration_date) values (150, 'MEAL', 2, 1, current_date, current_date + 300);
insert into card (amount, card_type, user_id, company_id, distribution_date, expiration_date) values (10, 'MEAL', 2, 1, current_date, current_date + 100);
insert into card (amount, card_type, user_id, company_id, distribution_date, expiration_date) values (20, 'GIFT', 2, 4, current_date, current_date + 20);
insert into card (amount, card_type, user_id, company_id, distribution_date, expiration_date) values (60, 'MEAL', 3, 4, current_date, current_date + 41);
insert into card (amount, card_type, user_id, company_id, distribution_date, expiration_date) values (40, 'GIFT', 3, 5, current_date, current_date + 10);