package com.challenge.glady;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class GladyApplication {

	public static void main(String[] args) {
		SpringApplication.run(GladyApplication.class, args);
	}

}
