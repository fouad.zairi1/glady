package com.challenge.glady.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DistributeCardDto {
    private String type;
    private int amount;
    private Long userId;
    private Long companyId;
}
