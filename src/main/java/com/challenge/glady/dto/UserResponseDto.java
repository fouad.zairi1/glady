package com.challenge.glady.dto;

import com.challenge.glady.utils.CardTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseDto {
    private String lastName;
    private String firstName;
    private Map<CardTypeEnum, Integer> balanceByCardType;
    private Integer availableBalance;
}
