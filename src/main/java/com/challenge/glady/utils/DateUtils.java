package com.challenge.glady.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;

public class DateUtils {
    public static Long getNumberOfDaysBetweenTwoDates(LocalDateTime localDateTime1, LocalDateTime localDateTime2) {
        LocalDate dateTime1 = localDateTime1.toLocalDate();
        LocalDate dateTime2 = localDateTime2.toLocalDate();
        return ChronoUnit.DAYS.between(dateTime1, dateTime2);
    }

    public static LocalDateTime getNextEndOfFebruary() {
        int currentYear = LocalDateTime.now().getYear();
        YearMonth nextYearFebruary = YearMonth.of(currentYear + 1, 2);
        return nextYearFebruary.atEndOfMonth().atTime(23, 59, 59);
    }
}
