package com.challenge.glady.controller;

import com.challenge.glady.dto.DistributeCardDto;
import com.challenge.glady.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/card")
@RequiredArgsConstructor
public class CardController {

    @Autowired
    CardService cardService;

    @PostMapping("/distribute")
    public ResponseEntity distributeCard(@RequestBody DistributeCardDto distributeCardDto) {
        try {
            return ResponseEntity.ok().body(cardService.distributeCard(distributeCardDto));
        } catch (IllegalArgumentException iae) {
            return ResponseEntity.badRequest().body(iae.getMessage());
        }
    }
}
