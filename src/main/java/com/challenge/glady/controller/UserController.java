package com.challenge.glady.controller;

import com.challenge.glady.dto.UserResponseDto;
import com.challenge.glady.model.User;
import com.challenge.glady.repository.UserRepository;
import com.challenge.glady.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;

    @GetMapping
    public ResponseEntity<List<User>> getAllUsers() {
        return ResponseEntity.ok().body((List<User>) userRepository.findAll());
    }

    @GetMapping("/{idUser}")
    public ResponseEntity getAmountByUser(@PathVariable Long idUser) {
        try {
            UserResponseDto response = userService.getMapBalanceByUser(idUser);
            return ResponseEntity.ok().body(response);
        } catch (IllegalArgumentException iae) {
            return ResponseEntity.badRequest().body(iae.getMessage());
        }
    }

}
