package com.challenge.glady.service;

import com.challenge.glady.model.Company;
import com.challenge.glady.model.User;
import com.challenge.glady.repository.CompanyRepository;
import com.challenge.glady.repository.UserRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Data
@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    public Company getCompany(Long id) {
        return companyRepository.findById(id)
                .orElseThrow(() -> new IllegalStateException("Could not find company"));
    }

    public void update(Company company) {
        companyRepository.save(company);
    }

    public List<Company> findAllCompanies() {
        return (List<Company>) companyRepository.findAll();
    }
}
