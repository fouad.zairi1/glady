package com.challenge.glady.service;

import com.challenge.glady.dto.DistributeCardDto;
import com.challenge.glady.model.Card;
import com.challenge.glady.model.Company;
import com.challenge.glady.repository.CardRepository;
import com.challenge.glady.utils.CardTypeEnum;
import com.challenge.glady.utils.DateUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Data
@Service
public class CardService {

    @Autowired
    private CardRepository cardRepository;
    @Autowired
    private CompanyService companyService;

    public Card distributeCard(DistributeCardDto distributeCardDto) {
        Company company = companyService.getCompany(distributeCardDto.getCompanyId());
        validateAvalaibleBalance(company, distributeCardDto.getAmount());
        company.setBalance(company.getBalance() - distributeCardDto.getAmount());
        companyService.update(company);
        return cardRepository.save(Card.builder()
                .amount(distributeCardDto.getAmount())
                .cardType(CardTypeEnum.valueOf(distributeCardDto.getType()))
                .userId(distributeCardDto.getUserId())
                .companyId(distributeCardDto.getCompanyId())
                .distributionDate(LocalDateTime.now())
                .expirationDate(calculateExpirationDateFromNow(CardTypeEnum.valueOf(distributeCardDto.getType())))
                .build());
    }

    private void validateAvalaibleBalance(Company company, int amount) {
        if (company.getBalance() < amount) {
            throw new IllegalArgumentException("Company " + company.getName() + " has not sufficient balance to buy this card");
        }
    }

    private LocalDateTime calculateExpirationDateFromNow(CardTypeEnum type) {
        switch (type) {
            case GIFT:
                return LocalDateTime.now().plus(365, ChronoUnit.DAYS);
            case MEAL:
                return DateUtils.getNextEndOfFebruary();
            default:
                return LocalDateTime.now();
        }
    }
}
