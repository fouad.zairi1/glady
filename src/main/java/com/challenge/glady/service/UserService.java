package com.challenge.glady.service;

import com.challenge.glady.dto.UserResponseDto;
import com.challenge.glady.model.Card;
import com.challenge.glady.model.User;
import com.challenge.glady.repository.UserRepository;
import com.challenge.glady.utils.CardTypeEnum;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User getUser(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Could not find User"));
    }

    public UserResponseDto getMapBalanceByUser(Long userId) {
        User user = getUser(userId);
        // get the available amount by card type
        Map<CardTypeEnum, Integer> balanceByCardType = getMapBalanceByCardType(user);

        // construct the response with users informations and global available balance
        return buildUserResponse(user, balanceByCardType);
    }

    private Map<CardTypeEnum, Integer> getMapBalanceByCardType(User user) {
        return user.getCardList()
                .stream()
                .filter(card -> card.getExpirationDate().isAfter(LocalDateTime.now()))
                .collect(Collectors.toMap(Card::getCardType, Card::getAmount, Integer::sum));
    }

    private UserResponseDto buildUserResponse(User user, Map<CardTypeEnum, Integer> balanceByCardType) {
        return UserResponseDto.builder()
                .lastName(user.getLastName())
                .firstName(user.getFirstName())
                .balanceByCardType(balanceByCardType)
                .availableBalance(balanceByCardType.values()
                        .stream()
                        .mapToInt(value -> value)
                        .sum())
                .build();
    }
}
