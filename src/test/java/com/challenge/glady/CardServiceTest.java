package com.challenge.glady;

import com.challenge.glady.dto.DistributeCardDto;
import com.challenge.glady.model.Card;
import com.challenge.glady.model.Company;
import com.challenge.glady.repository.CompanyRepository;
import com.challenge.glady.service.CardService;
import com.challenge.glady.utils.CardTypeEnum;
import com.challenge.glady.utils.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(value = MockitoExtension.class)
@SpringBootTest(classes = GladyApplication.class)
public class CardServiceTest {

    @Autowired
    public CardService cardService;
    @Autowired
    public CompanyRepository companyRepository;

    @Test
    public void should_distribute_one_gift_card() {
        DistributeCardDto distributeCardDto = DistributeCardDto.builder()
                .amount(100)
                .companyId(1L)
                .type(CardTypeEnum.GIFT.name())
                .userId(2L)
                .build();
        Card result = cardService.distributeCard(distributeCardDto);
        assertThat(result.getAmount()).isEqualTo(100);
        assertThat(DateUtils.getNumberOfDaysBetweenTwoDates(
                LocalDateTime.now().plus(365, ChronoUnit.DAYS),
                result.getExpirationDate()))
                .isEqualTo(0);

        // check if company balance is well updated
        Company company = companyRepository.findById(1L).get();
        assertThat(company.getBalance()).isEqualTo(5800);
    }

    @Test
    public void should_distribute_one_meal_card() {
        DistributeCardDto distributeCardDto = DistributeCardDto.builder()
                .amount(100)
                .companyId(2L)
                .type(CardTypeEnum.MEAL.name())
                .userId(2L)
                .build();
        Card result = cardService.distributeCard(distributeCardDto);
        assertThat(result.getAmount()).isEqualTo(100);
        assertThat(DateUtils.getNumberOfDaysBetweenTwoDates(
                DateUtils.getNextEndOfFebruary(),
                result.getExpirationDate()))
                .isEqualTo(0);

        // check if company balance is well updated
        Company company = companyRepository.findById(2L).get();
        assertThat(company.getBalance()).isEqualTo(7400);
    }

    @Test
    public void should_throw_exception_when_not_found() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
                DistributeCardDto distributeCardDto = DistributeCardDto.builder()
                        .amount(123456789)
                        .companyId(1L)
                        .type(CardTypeEnum.MEAL.name())
                        .userId(1L)
                        .build();
            cardService.distributeCard(distributeCardDto);
        });
    }
}
