package com.challenge.glady;

import com.challenge.glady.dto.UserResponseDto;
import com.challenge.glady.model.User;
import com.challenge.glady.service.UserService;
import com.challenge.glady.utils.CardTypeEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(value = MockitoExtension.class)
@SpringBootTest(classes = GladyApplication.class)
public class UserServiceTest {

    @Autowired
    public UserService userService;

    @Test
    public void should_get_user_by_id() {
        User user1 = userService.getUser(1L);
        assertThat(user1.getLastName()).isEqualTo("Pinkman");
        assertThat(user1.getCardList().size()).isEqualTo(3);
        assertThat(user1.getCardList().get(0).getAmount()).isEqualTo(100);
        assertThat(user1.getCardList().get(0).getCardType()).isEqualTo(CardTypeEnum.GIFT);
    }

    @Test
    public void should_throw_exception_when_not_found() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> userService.getUser(6L));
    }

    @Test
    public void should_get_map_balance_for_user() {
        UserResponseDto result = userService.getMapBalanceByUser(1L);
        assertThat(result.getAvailableBalance()).isEqualTo(220);
    }
}
