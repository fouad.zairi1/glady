# Test cases

Two possibilities for testing : 
* Starting the application and use postman. There is a collection : \src\main\resources\glady-challenge.postman_collection.json
* Unit tests

There is a company service/controller to ensure that company's is well updated after card distribution

User balance is calculated for each balance type (balanceByCardType) and total (availableBalance) => endpoint /users/{idUser}